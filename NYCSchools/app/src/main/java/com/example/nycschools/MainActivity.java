package com.example.nycschools;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.drm.ProcessedData;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    String url = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"; // url for web api to access
    RecyclerView recyclerView;                                            // custom recycler view created
    SchoolListAdaptor adaptor;                                            // adaptor used with recycler view
    ArrayList<SchoolList> schoolList;                                     // array to store json data in

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview);                         // create recycler view and pass it the adapter
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adaptor = new SchoolListAdaptor();
        recyclerView.setAdapter(adaptor);
        schoolList = new ArrayList<>();                                         // initialize the array
        getData();

    }


// this function gets the data from the api in json format and stores it as an array in the school list array
    private void getData() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");                                // shows message while json file is loading
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++) {                                   // iterates through entire json file until there is no more
                        JSONObject jsonObject = response.getJSONObject(i);
                        SchoolList schools = new SchoolList();
                        schools.setSchoolName(jsonObject.getString("school_name"));
                        schools.setAddress(jsonObject.getString("primary_address_line_1"));
                        schoolList.add(schools);                                                    // stores each name and address in the arrray

                    }
                }
                catch (JSONException e){
                    Toast.makeText(MainActivity.this,"JSON is not valid", Toast.LENGTH_SHORT).show();
                }
                adaptor.setData(schoolList);                                                     // passes the arrry to the adaptor so it can be used by recycler view
                adaptor.notifyDataSetChanged();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this,"Error Occurred", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);                   // uses volley to access the json file
        requestQueue.add(jsonArrayRequest);
    }
}

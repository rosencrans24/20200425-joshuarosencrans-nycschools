package com.example.nycschools;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;
// custom view holder to be used by the recycler view


public class SchoolListViewHolder extends RecyclerView.ViewHolder {

    TextView SchoolName;
    TextView Address;

    public SchoolListViewHolder(@NonNull View itemView) {
        super(itemView);

        SchoolName = itemView.findViewById(R.id.school_name);
        Address = itemView.findViewById(R.id.address);
    }
}

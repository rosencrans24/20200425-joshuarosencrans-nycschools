package com.example.nycschools;

// class created to store json data about schools when pulled from API
public class SchoolList {
    String SchoolName;
    String Address;

    public SchoolList() {
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }
}

package com.example.nycschools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SchoolListAdaptor extends RecyclerView.Adapter<SchoolListViewHolder> {

    ArrayList<SchoolList> schools;

    public SchoolListAdaptor() {
        schools = new ArrayList<>();
    }

    public void setData(ArrayList<SchoolList> schools) {
        this.schools = schools;
    }

    @NonNull
    @Override
    public SchoolListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View schoolView = layoutInflater.inflate(R.layout.recycler_row,parent,false);

        return new SchoolListViewHolder(schoolView);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolListViewHolder holder, int position) {

       SchoolList list = schools.get(position);
       holder.SchoolName.setText(list.SchoolName);
       holder.Address.setText(list.Address);


    }

    @Override
    public int getItemCount() {
        return schools.size();
    }
}
